# Directory structure

There is a data folder inside this git repository. Please [download](https://www.kaggle.com/c/pkdd-15-taxi-trip-time-prediction-ii/data) the data from kaggle, unzip it and put it in this folder. 

# Start


Please install all the necessary libraries. Python errors will guide you (panda, numpy, sklearn, folium)

then start the programm by simply running: python taxi.py

it will create an out folder with visualization data.

## Presentation

You can find and edit our presentation here:

https://drive.google.com/file/d/0B9Y0K3wHskA0dUEzc3ROM2o4OWM/view?usp=sharing
https://docs.google.com/presentation/d/1UldJD8H8Ds6cCUp39lC1_yQaPpWqleJQupune1H6Wvo/edit?usp=sharing