#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import sys
import random
import pandas as pd
import numpy as np

import os.path
import json
from numpy.polynomial.polynomial import polyline
from nearestneighborregressor import NearestNeighborRegressor 

def countlinenumbers(filename):
    with open(filename, 'rb') as csvfile:
        csv_read = csv.reader(csvfile, delimiter=',', quotechar='"')
        r=0
        for line in csv_read:
            r += 1
        return r


def split(filename, traindata_count, testdata_count, randomseed=69069, parts = 1):
    numberOfSamples = 1710670

    for part in range(parts):
        print "Splitting " + str(numberOfSamples) + " samples with seed: " + str(randomseed)
    
        lines2skip = np.random.choice(np.arange(1,numberOfSamples+1), (numberOfSamples-traindata_count-testdata_count), replace=False)
        print "linesToSkip=" + str(lines2skip)
        df = pd.read_csv(filename, skiprows=lines2skip)
        df.to_csv(filename[:-4] + str(traindata_count + testdata_count) + 'split-' + str(part) + '.csv', header=False, index=False, delimiter=',', quotechar='"')
        print "Wrote " + str(numberOfSamples - len(lines2skip)) + " lines in file: "+ filename[:-4] + str(traindata_count + testdata_count) + 'split' + str(part) + '.csv'

def parse_row(row):
    
    row_parsed = {"trip_id" : row[0],
                "call_type" : row[1],
                #"origin_call" : row[2],
                #"origin_stand" : row[3],
                #"taxi_id" : row[4],
                "timestamp" : int(row[5]),
                "day_type" : row[6],
                "missing_data" : True if row[7] == True else False,
                "polyline" : json.loads(row[8])}
    
    return row_parsed

def read_taxi_data(filename):
    """
        Reads csv file and parses features without splitting
    """
    features = []
    labels = []
    
    i = -1
    with open(filename, 'rb') as csvfile:
        csv_read = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csv_read:
            if i == -1:
                i += 1
                continue
            row_parsed = parse_row(row)
            features.append(row_parsed)
            labels.append((len(row_parsed["polyline"]) - 1) * 15)
            
            i += 1
    return (features, labels)


def read_split_taxi_data(filename, traindata_count, testdata_count, parts, part, seed=69069):
    """
        Divides csv file into train and test set.
    """
    train_features = []
    test_features = []
    train_labels = []
    test_labels = []
    random.seed(seed)

    if os.path.isfile(filename[:-4] + str(traindata_count + testdata_count) + 'split-'+str(part)+'.csv'):
        pass
    else:
        split(filename, traindata_count, testdata_count, seed, parts)

    filename = filename[:-4] + str(traindata_count + testdata_count) + 'split-'+str(part)+'.csv'

    with open(filename, 'rb') as csvfile:
        csv_read = csv.reader(csvfile, delimiter=',', quotechar='"')
        print "Reading test and train data from: " + filename
        i = 0;
        missing_values = 0
        too_small_polyline = 0
        too_big_polyline = 0
        cluster_count = 0
        print_cluster = (traindata_count + testdata_count) / 10 
        for row in csv_read:
            i+=1
            if row[7] == "False": # skip not complete data

                row_parsed = parse_row(row)

                row_parsed["polyline_uncropped"] = row_parsed["polyline"]
                
                if len(row_parsed["polyline"]) < 4: # skip if polyline is empty
                    too_small_polyline += 1
                    continue

                elif len(row_parsed["polyline"]) > 100:
                    too_big_polyline += 1
                    continue

                elif i <= traindata_count:
                    train_features.append(row_parsed)
                    train_labels.append((len(row_parsed["polyline"]) - 1) * 15)
                    
                    if i > (cluster_count+1) * print_cluster: 
                        print "Completed: Read " + str(int((float(i) / (traindata_count + testdata_count) * 100.0)))+ "% train data " # + "[" + str(i) + " - " + str(i-1+print_cluster) + "]"
                        cluster_count += 1
                    
                elif i <= traindata_count + testdata_count:
                    test_labels.append((len(row_parsed["polyline"]) - 1) * 15)
                    r = random.random()
                    crop = int(len(row_parsed["polyline"]) * r) # crop polyline in some "random" part

                    if crop < 0:
                        crop = 0
                    row_parsed["polyline"] = row_parsed["polyline"][:crop]
                    test_features.append(row_parsed)
                else:
                    print "Error: File was to big"
                    break
            else:
                #if polyline has missing values
                missing_values += 1
    

    print ">>Set has", str(missing_values), "missing values"
    print ">>Set has", str(too_small_polyline), "too small polylines (trips under a minute)"
    print ">>Set has", str(too_big_polyline), "too big polylines (trips over 30 minutes)"
    print ">>Output:", str(len(train_features)), "train features"
    print ">>Output:", str(len(test_features)), "test features"
    
    return (train_features, train_labels, test_features, test_labels)
