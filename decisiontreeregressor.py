from regressor import Regressor
import numpy as np
import math
from utils import calculate_initial_compass_bearing, haversine, get_average_speeds_during_day, generateTrafficMap
from datetime import datetime
from numpy.polynomial.polynomial import polyline
from sklearn.tree import DecisionTreeRegressor as dr

DAY_PARTS = 3

class DecisionTreeRegressor(Regressor):
    def __init__(self, K):
        self.K = K
 
    def generateFeatures(self, features):
        all_features = []
        
        for index, feature in enumerate(features):
            chosen_features = []


            if feature["call_type"] == "A":
                chosen_features.append(0)
            elif feature["call_type"] == "B":
                chosen_features.append(1)
            else:
                chosen_features.append(2)

            date = datetime.fromtimestamp(feature["timestamp"])
            chosen_features.append(self.trafficMap[int(date.hour)])
            
            all_features.append(chosen_features)
        
        return all_features

    def train(self, features, labels):
        
        self.trafficMap = generateTrafficMap(features, DAY_PARTS)

        chosen_features = self.generateFeatures(features)

        tree = dr(min_samples_leaf = self.K)
        tree = tree.fit(chosen_features, labels)
        
        self.model = {
            "labels" : labels,
            "features" : features,
            "tree" : tree
        }
    
    def apply(self, features):
        query_features = self.generateFeatures(features)
        result = []

        for index, test_feature in enumerate(query_features):
            pred = self.model["tree"].predict([test_feature])
            result.append((features[index]["trip_id"], pred[0]))
            
        return result
    
    