import math
import numpy as np
#from test.test_pep277 import filenames
from sklearn.metrics import mean_squared_error

class RegressorTester:
    def __init__(self, features, correct_labels, predicted_results):
        """
            @type features: list
            @param features: test features
            @type correct_labels: list
            @param correct_labels: list of correct labels
            @param predicted_results: list of tuples [(id, time), ...]
        """
        self.features = features
        self.correct_labels = correct_labels
        self.predicted_results = predicted_results

    
    def getQuality(self):
        #Measures quality of our Regressor.

        targets = np.array(self.correct_labels)
        predictions = np.array([pre[1] for pre in self.predicted_results])
        rmsle = np.sqrt(((np.log(predictions + 1) - np.log(targets + 1)) ** 2).mean())
        return rmsle 
    
    def generateSubmissionFile(self, filename):
        f = open(filename, 'w')
        f.write("TRIP_ID,\"TRAVEL_TIME\"\n")
        for predicted_result in self.predicted_results:
            f.write(predicted_result[0] + "," + str(predicted_result[1]) + "\n")
             
        f.close()
