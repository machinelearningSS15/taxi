from regressor import Regressor
import numpy as np
import math
from sklearn.neighbors import KDTree
from utils import calculate_initial_compass_bearing, haversine, get_average_speeds_during_day, generateTrafficMap
from datetime import datetime
from numpy.polynomial.polynomial import polyline

DIVIDE_POLYLINE_BY_DISTANCE = 500 # divides polyline by X meters
DIVIDE_POLYLINE_BY_TIMESLOTS = 8
DIVIDE_POLYLINE_TOLERANCE = 100

DAY_PARTS = 3

class NearestNeighborRegressor(Regressor):
    def __init__(self, K, endK):
        self.K = K
        self.endK = endK
        self.featureTrafficMap = False
        self.featureDirection = False
        self.featureCallType = False
 
    def generateFeatures(self, features, featureDirection=True, featureTrafficMap=True, featureCallType=True):
        chosen_features = []
        
        print "Generating features"
        if featureDirection == True:
            print "...using feature direction..."
        if featureTrafficMap == True:
            print "...using feature traffic map..."
            
        if featureCallType == True:
            print "...using feature call type..."
            
        for feature in features:
            if len(feature["polyline"]) > 1:
                feature["direction"] = calculate_initial_compass_bearing(tuple([feature["polyline"][0][1],feature["polyline"][0][0]]), tuple([feature["polyline"][-1][1],feature["polyline"][-1][0]]))
                if feature["direction"] < 0:
                    feature["direction"] += 360
                else:
                    feature["direction"] = None
        
        for index, feature in enumerate(features):
            
            #Feature #0 Shape
            if(feature["polyline"] != None and len(feature["polyline"]) > 1):
                startingPoint = feature["polyline"][0]
                feature["polyline_shape"] = [feature["polyline"][0]]
                feature["polyline_time_cut"] = feature["polyline"][0::DIVIDE_POLYLINE_BY_TIMESLOTS]
                
                currentDistance = 0
                currentPart = DIVIDE_POLYLINE_BY_DISTANCE
                
                for index_point, point in enumerate(feature["polyline"][1:]):
                    
                    lastAdd = haversine(feature["polyline"][index_point - 1][0], feature["polyline"][index_point - 1][1], point[0], point[1])
                    currentDistance += lastAdd
                    if currentDistance >= currentPart:
                        feature["polyline_shape"].append(point)
                        currentPart += DIVIDE_POLYLINE_BY_DISTANCE
            else:
                feature["polyline_shape"] = None
                feature["polyline_time_cut"] = None
            #Feature #1 Startpoint
            chosen_features.append(feature["polyline"][0])
            
            #Feature #2 Direction. Saved as tuple (degree, SW)
            if featureDirection == True:
                direction = calculate_initial_compass_bearing(tuple([feature["polyline"][0][1],feature["polyline"][0][0]]), tuple([feature["polyline"][-1][1],feature["polyline"][-1][0]]))
                
                coordNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"]
                coordIndex = int(round(direction / 45))
                if coordIndex < 0:
                    coordIndex = coordIndex + 8
                feature["direction"] = (coordNames[coordIndex], coordIndex ,direction)
                chosen_features[index].append(coordIndex/8)


            # feature #3 traffic map
            if featureTrafficMap == True:
                date = datetime.fromtimestamp(feature["timestamp"])
                daytime_traffic = self.trafficMap[int(date.hour)]
                chosen_features[index].append(daytime_traffic)


            #Feature #4 - call type
            if featureCallType == True:
                call_type_map = {   "A": 0.0,
                                    "B": 1.0,
                                    "C": 2.0}
                
                chosen_features[index].append(call_type_map[feature["call_type"]])      
            
        chosen_features = np.array(chosen_features, np.float32)
        
        return chosen_features

    ## 15.06.15 new function for every adaption. so that we can document it 
    def generateFeatures2(self, features):
        chosen_features = []
        for index, feature in enumerate(features):
            
            #Feature #1 Startpoint
            chosen_features.append(feature["polyline"][0])
            
            #Feature #2 Direction. Saved as tuple (degree, SW)
            direction = calculate_initial_compass_bearing(tuple([feature["polyline"][0][1],feature["polyline"][0][0]]), tuple([feature["polyline"][-1][1],feature["polyline"][-1][0]]))
            
            coordNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"]
            coordIndex = int(round(direction / 45))
            if coordIndex < 0:
                coordIndex = coordIndex + 8
            feature["direction"] = (coordNames[coordIndex], coordIndex ,direction)
            chosen_features[index].append(coordIndex)


            date = datetime.fromtimestamp(feature["timestamp"])
            
            trafficmap = {   "0" : 2,
                             "1" : 2,
                             "2" : 3,
                             "3" : 3,
                             "4" : 3,
                             "5" : 3,
                             "6" : 3,
                             "7" : 3,
                             "8" : 2,
                             "9" : 1,
                             "10" : 1,
                             "11" : 1,
                             "12" : 1,
                             "13" : 1,
                             "14" : 1,
                             "15" : 1,
                             "16" : 1,
                             "17" : 1,
                             "18" : 0,
                             "19" : 0,
                             "20" : 1,
                             "21" : 1,
                             "22" : 2,
                             "23" : 2}

            daytime_traffic =trafficmap[str(date.hour)]
            #chosen_features[index].append(float(daytime_traffic))
            #Feature #3 
            #...
            call_type_map = {   "A": 0.0,
                                "B": 1.0,
                                "C": 2.0}
            
            #chosen_features[index].append(call_type_map[feature["call_type"]] / 3)   
        print chosen_features    
        
        chosen_features = np.array(chosen_features, np.float32)
        
        return chosen_features
    ## new function for every adaption. so that we can document it 
    def generateFeatures3(self, features):
        chosen_features = []
        for index, feature in enumerate(features):
            
            #Feature #1 Startpoint
            chosen_features.append(feature["polyline"][0])
            
            #Feature #2 Direction. Saved as tuple (degree, SW)
            direction = calculate_initial_compass_bearing(tuple([feature["polyline"][0][1],feature["polyline"][0][0]]), tuple([feature["polyline"][-1][1],feature["polyline"][-1][0]]))
            
            coordNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"]
            coordIndex = int(round(direction / 45))
            if coordIndex < 0:
                coordIndex = coordIndex + 8
            feature["direction"] = (coordNames[coordIndex], coordIndex ,direction)
            chosen_features[index].append(coordIndex/8)


            date = datetime.fromtimestamp(feature["timestamp"])

            #Feature #3 
            #...
            call_type_map = {   "A": 0.0,
                                "B": 1.0,
                                "C": 2.0}
            
            chosen_features[index].append(call_type_map[feature["call_type"]])      
            
        chosen_features = np.array(chosen_features, np.float32)
        
        return chosen_features
   
    def train(self, features, labels):
        
        #if self.featureTrafficMap == True:
        self.trafficMap = generateTrafficMap(features, DAY_PARTS)

        chosen_features = self.generateFeatures(features, self.featureDirection, self.featureTrafficMap, self.featureCallType)

        tree = KDTree(chosen_features, leaf_size=30)
        
        self.model = {
            "labels" : labels,
            "features" : features,
            "tree" : tree
        }
    
    def apply(self, features):
        #print self.model["labels"]
        result = []
        self.neighbourtrips = []
        # todo pass all the features at once to kdtree
        #query_features = self.generateFeatures3(features)
        query_features = self.generateFeatures(features, self.featureDirection, self.featureTrafficMap, self.featureCallType)

        for index, test_feature in enumerate(features):

            dist, neighbours = self.model["tree"].query(query_features[index], k=self.K)
            dist = dist[0] # it's in format [[dist, dist, ...]] for some reason

            neighbours = neighbours[0] # it's in format [[0, 5245, anotherneighbour...]] for some reason

            neighbours_labels = []
            
            neighbours_features = []
            for neighbour_index in neighbours:
                neighbours_features.append(self.model["features"][neighbour_index])

            neighbours = self.clever_distance_method(test_feature, neighbours_features)
            for neighbour in neighbours[:self.endK]:
                neighbours_labels.append((len(neighbour["polyline"]) - 1) * 15)
                #train_start_point = self.model["features"][neighbour]["polyline"][0]
                #test_start_point = test_feature["polyline"][0]
                
                #distance = haversine(train_start_point[0], train_start_point[1], test_start_point[0], test_start_point[1])
                    
                #if distance < 50:
                #    distance = 50
                
                #neighbours_weighted_distances.append(1.0)    
                #neighbours_weighted_distances.append(1.0 / math.pow(distance, 2))    
            

            average = np.ma.average(neighbours_labels, axis=0)
            #for endpoint do this
            #average = int(round(average / self.K)) + (len(test_feature["polyline"]) - 1) * 15
            result.append((test_feature["trip_id"], average))
            
            realneighbours = neighbours[:self.endK]
            

            self.neighbourtrips.append(realneighbours)
            
        return result
    
    def clever_distance_method(self, test_feature, neighbours):

        date = datetime.fromtimestamp(test_feature["timestamp"])
        test_feature["traffic_map"] = self.trafficMap[int(date.hour)]

        if len(test_feature["polyline"]) > 1:
            test_direction = calculate_initial_compass_bearing(tuple([test_feature["polyline"][0][1],test_feature["polyline"][0][0]]), tuple([test_feature["polyline"][-1][1],test_feature["polyline"][-1][0]]))
        else:
            test_direction = None
        

        for neighbour in neighbours:
            points_difference_total = 0.0
            
            polyline_simmilarity = "polyline_time_cut" # "polyline_shape"
            if test_feature[polyline_simmilarity] == None or neighbour[polyline_simmilarity] == None or len(test_feature[polyline_simmilarity]) > len(neighbour[polyline_simmilarity]):
                neighbour["points_difference"] = None
            else:
                len_test = len(test_feature[polyline_simmilarity])
                len_train = len(neighbour[polyline_simmilarity])
                check_points = len_test if len_test < len_train else len_train 
                for index in range(check_points):
                    points_difference = haversine(test_feature[polyline_simmilarity][index][0], test_feature[polyline_simmilarity][index][1], neighbour[polyline_simmilarity][index][0], neighbour[polyline_simmilarity][index][1])
                    if points_difference > DIVIDE_POLYLINE_TOLERANCE:
                        points_difference_total += points_difference

                neighbour["points_difference"] = points_difference_total

        max_difference = max([x["points_difference"] for x in neighbours])

        for neighbour in neighbours:
            date = datetime.fromtimestamp(neighbour["timestamp"])
            daytime_traffic = self.trafficMap[int(date.hour)]
            neighbour["traffic_map"] = daytime_traffic
          
            if neighbour["points_difference"] == None:
                polyline_simmilarity = 0.0
            elif max_difference != 0.0:
                polyline_simmilarity = 1.0 - neighbour["points_difference"] * 1.0 / max_difference
            else:
                polyline_simmilarity = 1.0

            if test_direction != None and neighbour["direction"] != None:
                if test_direction < 0.0:
                    test_direction += 360
                
                difference = test_direction - neighbour["direction"];
                if (difference < -180):
                    difference += 360
                if (difference > 180):
                    difference -= 360;
                
                direction_simmilarity = 1.0 - abs(difference) * 1.0 / 180
            else:
                direction_simmilarity = 0.0

            call_type_simmilarity = 1.0 if neighbour["call_type"] == test_feature["call_type"] else 0.0
                
            traffic_map_simmilarity = 1 - abs(test_feature["traffic_map"] - neighbour["traffic_map"]) * 1.0 / (DAY_PARTS - 1)

            neighbour["simmilarity"] = 5 * polyline_simmilarity + 3 * direction_simmilarity + traffic_map_simmilarity #+ call_type_simmilarity
            
        neighbours.sort(key=lambda x: x["simmilarity"], reverse=True)
        
        return neighbours