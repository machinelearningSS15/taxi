from regressor import Regressor
import numpy as np
import math
from sklearn.neighbors import KDTree
from utils import *
from datetime import datetime
import collections

DAY_PARTS = 3

class MultipleKDTreeRegressor(Regressor):
    def __init__(self, K_Final_NBs = 100, K_Tree_NBs = 5000, tree_splits_in_minutes = [0, 1, 2, 3, 4, 5, 7, 9, 11, 15]):
        self.K_Final_NBs = K_Final_NBs
        self.K_Tree_NBs = K_Tree_NBs
        self.tree_splits_in_minutes = tree_splits_in_minutes

    def train(self, features, labels):

        self.model = {
            "labels" : labels,
            "features" : features,
            "tree_to_feature_map" : [],
            "treefeatures" : [],
            "tree" : []
        }
        
        for feature in features:
            if len(feature["polyline"]) > 1:
                feature["direction"] = calculate_initial_compass_bearing(tuple([feature["polyline"][0][1],feature["polyline"][0][0]]), tuple([feature["polyline"][-1][1],feature["polyline"][-1][0]]))
                if feature["direction"] < 0:
                    feature["direction"] += 360
            else:
                feature["direction"] = None
        self.trafficMap = generateTrafficMap(features, DAY_PARTS)


        for i in self.tree_splits_in_minutes:

            #For every subroute we create a KD-Tree with start and end point
            point_needed = int(i * 4)
            chosen_features = []
            
            tree_to_feature_map = {}
            treeindex = 0

            treefeatures = []

            for index, feature in enumerate(features):

                #Only if the train trip has the requiered length
                if len(feature["polyline"]) > point_needed:                    
                    f = []
                    f.append(feature["polyline"][0][0])
                    f.append(feature["polyline"][0][1])
                    if(point_needed != 0):
                        f.append(feature["polyline"][point_needed][0])
                        f.append(feature["polyline"][point_needed][1])
                    tree_to_feature_map[str(treeindex)] = index
                    chosen_features.append(f)
                    treeindex += 1
                    treefeatures.append(feature)

            chosen_features = np.array(chosen_features, np.float32)
            self.model["tree"].append(KDTree(chosen_features, leaf_size=30))
            self.model["treefeatures"].append(treefeatures)

    def apply(self, features):

        #print self.model["labels"]
        result = []
        self.neighbourtrips = []

        #iter through test cases
        for index, test_feature in enumerate(features):
            test_case_result = []

            #for every test trip we have to compute every kd tree
            for tree, i in enumerate(self.tree_splits_in_minutes):

                point_needed = int(i * 4)

                #Only if the train trip has the requiered length
                if len(test_feature["polyline"]) > point_needed:
                    #create query
                    query = []
                    query.append(test_feature["polyline"][0][0])
                    query.append(test_feature["polyline"][0][1])
                    if(point_needed != 0):
                        query.append(test_feature["polyline"][point_needed][0])
                        query.append(test_feature["polyline"][point_needed][1])
                    
                    query = np.array(query, np.float32)

                    #query the right tree
                    dist, neighbours = self.model["tree"][tree].query(query, k = self.K_Tree_NBs)
                    dist = dist[0] # it's in format [[dist, dist, ...]] for some reason
                    neighbours = neighbours[0] # it's in format [[neighbour, neighbour, ...]] for some reason
                    test_case_result.append(neighbours)
                else:
                    #print "Test Case", str(index), "is shorter than", str(self.tree_splits_in_minutes[tree]), "minutes"
                    pass

            ##do clever distance measure
            neighbors_for_trees = []
            for t, tree_res in enumerate(test_case_result):
                neighbors_for_trees.append([self.model["treefeatures"][t][aN] for aN in tree_res])
            
            final_neighbors = self.clever_distance_method(test_feature, neighbors_for_trees)

            #after
            neighbours_labels = []


            for final_neighbor in final_neighbors[:self.K_Final_NBs]:
                neighbours_labels.append(duration_from_polyline(final_neighbor["features"]["polyline"]))    

            realneighbours = [n["features"] for n in final_neighbors[:self.K_Final_NBs]] 

            self.neighbourtrips.append(realneighbours)

            average = np.ma.average(neighbours_labels, axis=0)
            result.append((test_feature["trip_id"], average))

        return result

    def clever_distance_method(self, test_feature, neighbors_for_trees):
        processed_trip_ids = {}
        processed_trip_ids_list = []
        i = 0
        
        date = datetime.fromtimestamp(test_feature["timestamp"])
        daytime_traffic = self.trafficMap[int(date.hour)]
        test_feature["traffic_map"] = daytime_traffic
        if len(test_feature["polyline"]) > 1:
            test_direction = calculate_initial_compass_bearing(tuple([test_feature["polyline"][0][1],test_feature["polyline"][0][0]]), tuple([test_feature["polyline"][-1][1],test_feature["polyline"][-1][0]]))
        else:
            test_direction = None
        # compute advanced features
        for index_tree, neighbours_for_tree in enumerate(reversed(neighbors_for_trees)):
            for index_neighbour, neighbour in enumerate(neighbours_for_tree):
                if processed_trip_ids.has_key(neighbour["trip_id"]):
                    processed_trip_ids_list[processed_trip_ids[neighbour["trip_id"]]]["tree_count"] += 1
                else:
                    date = datetime.fromtimestamp(neighbour["timestamp"])
                    daytime_traffic = self.trafficMap[int(date.hour)]
                    neighbour["traffic_map"] = daytime_traffic
                    processed_trip_ids[neighbour["trip_id"]] = i
                    processed_trip_ids_list.append({"tree_count" : 1, "features": neighbour})
                    i += 1
        
        # compute distances
        for processed_trip in processed_trip_ids_list:
            tree_count_simmilarity = processed_trip["tree_count"] * 1.0 / len(self.tree_splits_in_minutes);
            # not relevant -- call_type_simmilarity = 1.0 if test_feature["call_type"] == processed_trip["features"]["call_type"] else 0.0
            # not relevant -- traffic_map_simmilarity = 1 - abs(test_feature["traffic_map"] - processed_trip["features"]["traffic_map"]) * 1.0 / (DAY_PARTS - 1)
            if test_direction != None and processed_trip["features"]["direction"] != None:
                if test_direction < 0.0:
                    test_direction += 360
                
                difference = test_direction - processed_trip["features"]["direction"];
                if (difference < -180):
                    difference += 360
                if (difference > 180):
                    difference -= 360;
                
                direction_simmilarity = 1.0 - abs(difference) * 1.0 / 180
            else:
                direction_simmilarity = 0.0
            
            processed_trip["simmilarity"] = 4 * tree_count_simmilarity + direction_simmilarity  
        
        processed_trip_ids_list.sort(key=lambda x: x["simmilarity"], reverse=True)
        
        return processed_trip_ids_list
