%----------------------------------------------------------------------------
%
% Bitte verwenden Sie dieses Template für die schriftliche
% Ausarbeitung im Fachseminar zum Thema Digitale Bildverarbeitung
%
% Weitere Informationen (Beispiel, Tutorial, ...) zu Latex finden Sie unter
% http://www.dante.de/faq/de-tex-faq/ (Deutschsprachige Anwendervereinigung TeX)
% http://www.ctan.org/ (Comprehensive TeX Archive Network) 
% http://www.latex-project.org/ (official website of LaTeX 3 project)
%
%----------------------------------------------------------------------------
\documentclass[ a4paper, twocolumn]{article}
                                
\usepackage[latin1]{inputenc}
\usepackage[pdftex]{graphics}
\usepackage[iso]{umlaute}
%\usepackage{ngerman}
\usepackage{graphicx} 
\usepackage{layout} 
\usepackage{geometry}
\usepackage{fancyhdr}
\usepackage{expdlist}
\usepackage{makeidx}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{listings}
\usepackage{amsmath,amssymb,amstext}
\def\code#1{\texttt{#1}}

\fancyfoot[C]{\thepage}%  Spezielle Fusszeile


\begin{document}

%----------------------------------------------------------------------------
% BEGIN Kopfzeile
\title{\textbf{Taxi trip time prediction}}

\author{ \textit{Autor} \\ Ales Kajzar, Johannes Villmow \\  alesh.kajzar@gmail.com, johannes@villmow.de\texttt{}
}

\date{20. July 2015} 
\maketitle
%   END Kopfzeile
%----------------------------------------------------------------------------


%----------------------------------------------------------------------------
% BEGIN Abstract
\section*{Abstract}

In this paper we present our approach to solve a machine learning challenge from kaggle. Building a classifier, or a regressor is always an incremental process. When you start you need to first understand the challenge, then understand the data your working with and after that you can start to try out different approaches to solve the challenge. 

We have chosen a challenge where we had to predict the total travel time of taxi trips based on their initial trajectories. We will present our approaches on understanding the data, that includes visualization and the different machine learning algorithms we used to solve the challenge.



% END Abstract
%----------------------------------------------------------------------------


%----------------------------------------------------------------------------
% BEGIN Introduction
\section{Introduction}
\label{sec:introduction}

\subsection{Challenge}

Our task was to predict the total travel time of taxi trips based on their initial partial trajectories.\\

As a taxi company you need to know how long your taxi drivers will be occupied. This would be easy to calculate, if one would know the destination of a taxi trip. The problem is, that most of the taxi drivers don't give their destination to the central. If a taxi dispatching system would know the approximate time a driver would be ending their current ride, they could better plan with them.\\

The challenge is to build a machine learning system, that is able to infer the travel time of a taxi trip, based on the trips (initial) partial trajectory. The data for the taxi trips comes from Porto, Portugal.\footnote{https://www.kaggle.com/c/pkdd-15-taxi-trip-time-prediction-ii} 


\subsection{Data}

We were given two datasets: A training set, containing data from 1.710.670 taxi trips in Porto and a testing set with 320 taxi trips, for which we had to predict the total travel time. Furthermore we got metadata describing the taxi stands.
The result could be then uploaded to kaggle. They will then run an evaluation script on it and give us a quality measure how well our regressor works.\\

The structure of the training and testing dataset provided by kaggle contains 9 features: 

\begin{enumerate}
    \item[1.] TRIP\_ID
        % \begin{description}
        %     A numerical number identifying a single trip.
        % \end{description}
    \item[2.] CALL\_TYPE
        \begin{enumerate}
            \item['A': ] Taxi Central Based. Then the data contains an anonymized unique identifier of the callers phone number (3.~ORIGIN\_CALL).
            \item['B': ] Stand-based. Then the data contains a field with an unique identifier of the taxi stand (4. ORIGIN\_STAND).
            \item['C': ] Non-taxi central based
        \end{enumerate}
    \item[5.] TIMESTAMP
    \item[6.] DAYTYPE
        \begin{itemize}
            \item['A': ] Normal day
            \item['B': ] If this trip started on a holiday or any other special day (extending holidays, floating holidays, etc.)
            \item['C': ] If the trip started on a day before a type B day
        \end{itemize}
  \item[7.] POLYLINE 
    \begin{description}
      It contains a list of GPS coordinates (i.e. WGS84 format) mapped as a string. The beginning and the end of the string are identified with brackets (i.e. [ and ], respectively). Each pair of coordinates is also identified by the same brackets as [LONGITUDE, LATITUDE]. This list contains one pair of coordinates for each 15 seconds of trip. The last list item corresponds to the trip's destination while the first one represents its start.
    \end{description}
    \item[8.] MISSING\_DATA
    \item[9.] TAXI\_ID 
\end{enumerate}

\subsection{Training and Testing Set}

The training set provided by kaggle contained data of 1.710.670 taxi trips in Porto. This was more than our computers could handle at once. 
Furthermore the test set provided by kaggle contained only 320 test trips and the results could only be evaluated online on kaggle, because we didn't know the labels (lenghts of trips). There was a limitation, that you could only upload two sample submissions per day. From those submissions of all users, that participated in this competition a leatherboard was build. 

So we had to create our own training set and a new test set, to be able to see how well our regressor works without using online kaggle evaluation and then we submitted only the best results we could get. We found out that our computers couldn't handle sets bigger than 800.000 trips. To be able to get the results in an acceptable time, we have decided to split into 400.000 train trips (\code{traindata\_count}) and 2.000 test trips (\code{testdata\_count}). We have also found out that twice as much train trips and more test trips did not affect final results too much (the difference was about 0.01). \code{traindata\_count} and \code{testdata\_count} were implemented as global variables, so that we could experiment with different split sizes. The splitted dataset was stored in a single CSV file, named \code{train402000split.csv}.

Therefore we wrote a little method that splits the CSV file into a smaller one, if there is no splitfile in the data folder, with the size of \code{traindata\_count + testdata\_count}. The original training set was ordered by date of the trip. We couldnt just split it in half, because then we would loose necessary information.
%what necessary information???
A list with random line numbers, created with a seed to assure reproducability, was the solution to this problem. This list was passed to pandas \code{read\_csv} method for the \code{skiprows} attribute. After this was done, we exported the panda object to a csv file. 

\subsubsection{Crossvalidation}
To be able to do crossvalidation, we created three different train and test sets of the size mentioned above. Our regressor was then started three times each time on a different training and test set just to be sure, that this train and test set is not an "unusual" one.


\subsection{Quality Measure}

To evaluate how well our regressor performs, we needed to upload a submission.csv file to kaggle. For each trip, identified by its trip id, there is a corresponding travel time in seconds.
\texttt{
TRIP\_ID,TRAVEL\_TIME \\
T1,60 \\
T2,90 \\
T3,122 \\
etc.
}

As a quality measure we were told to use the Root Mean Squared Logarithmic Error (RMSLE). The RMSLE is calculated as 

\begin{equation}
     \sqrt{ \frac{ 1 }{ n } \sum\limits_{i=1}^n (log{(p_i + 1)} - log{(a_i + 1)} )^{2}} 
\end{equation}
Where: 
\begin{itemize}
    \item $n$ is the number of trips in the test set 
    \item $p_i$ is our predicted count
    \item $a_i$ is the actual count
    \item $log(x)$ is the natural logarithm
\end{itemize}

After every approach we calculated the RMSLE, to see how well our regressor performed on our testing set. After we found the approach that worked best on our test set, we uploaded our result to kaggle. But we had to notice, that the RMSLE we got from kaggle could differ a lot to our RMSLE on our test set.    

\section{Data Analysis}

Due to the big amount of taxi trips as a training set, we had to determine which features are useful. Therefore we analysed the data and we found out, that only minimum trips have their duration under a minute or over 25 minutes. That is why we dropped them from train set, because those trips affected negatively our results (our first submitted result was much worse than simple averaging all the trips). The charts below does not contain this data already.


\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{images/trip_duration_distribution.jpg}
    \caption{Trip duration distribution}
    \label{fig1}
\end{figure}

As you can see in figure \ref{fig1}, there is a big difference between probability of "average" trips (average value is 660 seconds) and the shortest or the longest trips.


\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{images/day_type_distribution.jpg}
    \caption{Day type distribution}
    \label{fig2}
\end{figure}

Training set contained also a value \texttt{DAY\_TYPE}, which should distinguish between holidays, and ordinary days. But, as you can see in figure \ref{fig2}, there is practically no reason to consider different day types, because all of our train trips had one type.


\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{images/call_type_distribution.jpg}
    \caption{Call type distribution}
    \label{fig3}
\end{figure}

Figure \ref{fig3} shows probabilities of time durations of trips with different call types (A, B and C). We had a clue that using \texttt{CALL\_TYPE} feature could positively affect our results, because according to this chart are probabilities of time ranges of trips with call type C more similar than by the other call types (for example difference between range 60-200 and 200-400 is by call type C the lowest).

We also looked at average times, distances and speeds during hours of a day (00:00 to 23:00).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{images/average_time.jpg}
    \caption{Average time of trips during a day}
    \label{avg-time}
\end{figure}

Figure \ref{avg-time} shows, that the longest trips are probably in the evening, during moring they are shorter (and the difference between 19:00 and 03:00 could mean, that we found a strong feature).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{images/average_distance.jpg}
    \caption{Average distance of trips during a day}
    \label{avg-dist}
\end{figure}

Even though the average times in the morning are the shortest, average distance of trips are in the morning the highest.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{images/average_speed.jpg}
    \caption{Average speed of trips during a day}
    \label{avg-speed}
\end{figure}

Average speeds during a day differs a lot as well.

\section{Visualization}

\begin{figure*}[ht]
    \centering
    \includegraphics[width=\textwidth]{images/visualization.png}
    \caption{Our final visualization}
    \label{figv}
\end{figure*}

As we are using regression on taxi trips it is obvious to think about visualization of the trips on a map. But first we asked ourselves, what do we want to visualize. Our self made test set contained 2.000 trips. Visualizing each of them could be very confusing. We wanted to be able to choose which test trip, we want to see on a map. The path of this trip should be drawn on the map, with a marker for the start point and one for the end point. Furthermore we'd need the total duration of this test trip and the result of our regressor compared to it. If we use k-nearest neighbors as an algorithm, we want to be able to see the k-neighbors, their duration and their path on the map as well.

\subsection{Implementation}
For implementation we used a library called \code{folium}\footnote{https://pypi.python.org/pypi/folium}, which lets us create an html file with an interactive OpenStreetMap map, with just a few lines of python code. We wrote a function, that lets us print a trip on a given map. Another helper function will create a map and print a single test trip, with the k-neighbors on it. To distinguish between the neighbours, there is a gradient from green to red. This gradient reflects the distance of a neighbor compared to the others. From this map \code{folium} will produce a single html file showing the map. 

As it is sometimes necessary to see more than one test trip, we expanded the functionality of our visualization. A single html file showing only one map is not enough, because folium doesn't have the option to control interactively what's on the map. Therefore we expanded our visualizer, so that it can take the whole test set. For every test trip it creates an html file, showing the trip itself and the k-neighbors. These files are dynamically build into another html file, acting as a index-page. It shows the overall RMSLE quality measure for this test set, a list of all the test trips, including our predicted time and the actual time. Once you click on a test trip, the corresponding html file will show up. Now we were able to choose interactively in our browser which test trip we want to visualize. 

\subsection{Final visualization}

Our final visualization is shown in figure \ref{figv}. On the left side you can see the different test trips. The bold duration is the actual travel time, the time right next to it is our predicted result. On the top you can see our RMSLE score. Once you click on the map button a map will load on the right side. 
There you can see a blue line displaying the full (not the cropped) path of the chosen test trip. The big orange circle is marking the start point and the small blue circle the end point of this test trip. The smaller orange circle on the blue path marks the point, where we cropped the polyline so that we could get accurate test data.
The light blue circles show starting points and the small green ones the end point of the found neighbors. The color indicates how good these neighbors were, based on their duration. You are able to see the duration of every neighbor, by clicking on the green end point. A pop-up will appear and show you information about this trip.

\section{Solution approaches}

As we started with the project we had several ideas how we could implement our machine learning system. In a first weeks we focused on picking right features instead of picking the right regressor, so we used mostly k-NN algorithm. For speeding it up we have used kd-tree.

\subsection{Single k-Nearest neighbour}

As a first step we used k-nearest neighbor algorithm with several combinations of possible features, and we looked how far we could get with this. Due to the big amount of data, we used kd-tree out of the sklearn package for speedup. We considered these features:

\begin{itemize}
    \item Start point (longitude and latitude coordinates)
    \item Direction between start and end point (reduced to 8 directions)
    \item Call type
    \item "Traffic map" (using hour-of-day time average we assign the trip to 3 categories 0, 1, 2; 0 is for the hours of day, which have shortest trips, 2 for the longest)
\end{itemize}


Using different combinations of these features we always got $K$ nearest neighbours (we started at $K=20$ and added always 5 until the evaluation function gave us smaller improvement than 0.01).

At first we used weighted average to calculate the result, but then we started to analyze if just average is better. We found out that it doesn't really matter. Weighted average was a little bit better in the beginning, but sometimes it was worse.

\subsection{Feature Selection}

We tested all combination of features with our own testing set, but got very similar results. Because we could only upload two scripts to kaggle per day, we couldn't analyse for every combination how well it worked.

\begin{table}[h]
\centering
\caption{Evaluation using different features}
\label{eval-single}
\begin{tabular}{lll}
Features      & Our   & Kaggle \\ \hline
SP            & 0.534 & 0.892  \\
SP, D         & 0.533 & -      \\
SP, D, TM     & 0.539 & -      \\
SP, D, CT     & 0.534 & -      \\
SP, TM, CT    & 0.534 & -      \\
SP, D, TM, CT & 0.536 & 0.854 
\end{tabular}


SP = start point, D = direction, TM = traffic map, CT = call type
\end{table}

In table \ref{eval-single} you can see that at our test set the features didn't really mater. When we used all features the result was even worse, than using just the starting point. But at kaggle this made a big difference. So we considered that maybe those features are not the best to choose. 

\subsection{Multiple k-nearest neighbour}

Our next idea was to take the route of the test trip in account. Till know we only used the most simple features to find similar trips. We wanted to expand our kd-tree so that we can find trips, that took approximately the same route and also same time as our test trip. A problem with using kd-trees is, that a kd-tree needs a fixed amount of features for training and testing. Our taxi trips have a variable length, so we can't just give the full polyline to our kd-tree model.

\subsubsection{Implementation}

So far we have used a single kd-tree to do an efficient nearest neighbor regression. This way we had always a fixed amount of features. The idea we came up with, was that we do a 2-step nearest neighbor:
\begin{enumerate}
\item Find trips that have a similar path to the test trip
\item Rank these neighbors, using a self-made similarity measure (call type, traffic map, \dots)
\end{enumerate}

To find trips that have a similar path to our test trip, we used multiple kd-trees ($i$ is the amount of trees). These trees were constructed offline and each tree was trained on two features: starting point, and the point after $n$ minutes. The first tree had only the starting point in its model. The second tree for example could use the point after 1 minute as the second feature, and so on. So every other kd-tree had a subset of the trips of the first tree stored in its model. If a tree is shorter than $n$ minutes, it's not part of the $n$ minute kd-tree.\\ 

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{images/multikdtree.png}
    \caption{Model of multiple kd-tree}
    \label{mkdtree}
\end{figure}

Once we want to predict the travel time of a test trip, we look at the time it already traveled. We then use all kd-trees, with $n$ shorter than the time of the test trip. From every tree we get $k$-neighbors identified by its \code{TRIP\_ID}. Those are the neighbors, that have a similar starting point and a similar point at time point $n$. If we would find a trip that would have the identical path, it would come out of every tree as a possible neighbor. The best neighbors are obviously those, that are nearest neighbors in multiple kd-trees. Based on this we can do a first ranking and reduce the amount of neighbors from $i*k$ to $k$ again.  \\

After we applied this to a test trip we got $k$ neighbors that have a similar path to our test trip. Now we can take other features like day type, call type and so on into account. 

\subsubsection{Cross-validation}

As on every nearest neighbor you have to find the optimal $k$ using cross-validation. With our approach there are multiple $k$ values. The first parameter is how many neighbors each kd-tree will find in the first step. This parameter should be high, because we want to find a lot of neighbors for every time point. These neighbors don't have to be the most similar, its just important that we get enough of them, to be able to find the ones that are in multiple trees. We did a little bit of testing and found out that 5.000 neighbors for each tree is a good number.
The second parameter determines how many of these $i * 5.000$ neighbors we use to estimate the time. To estimate the time we simply used an average of the $k$ best neighbors, determined by our similarity measure.

Furthermore for this approach we had to find out the optimal amount of kd-trees (so the amount of time points). The more kd-trees we use, the more accurate will be the path, but computation time will increase as well. We experimented with three different time points:

\begin{enumerate}
\item[E1:] 0, 5, 10, 15 minute points
\item[E2:] 0, 2, 5, 8, 10, 15 minute points
\item[E3:] 0, 1, 2, 3, 4, 5, 7, 9, 11, 15 minute points
\end{enumerate}

The results of these different approaches were evaluated at kaggle, to determine how many neighbors to use and which amount of kd-trees is the best. The results are shown in table \ref{multikd_result}

\begin{table}[h]
\centering
\caption{Crossvalidation of multiple kd-trees}
\label{multikd_result}
\begin{tabular}{llll}
k   & E1 & E2 & E3 \\ \hline
25  & -               & -                        & 0.68148                     \\
50  & 0.71712         & 0.70969                  & \textbf{0.67989}            \\
100 & 0.71530         & 0.70879                  & 0.69071                     \\
200 & -               & -                        & 0.70101                    
\end{tabular}
\end{table}

As you can see using 50 neighbors lead to the best result, without taking other features in concern. This table was made using only the shape of the path as a feature. We then started to evaluate which features will improve our multi kd-tree. These features were included in our similarity measure in the second step. 

\subsubsection{Evaluation}

As we can see, using the full path as a feature lead to better results on kaggle than our first approach. This approach is actually just an optimization of the single kd-tree, to not only use the starting point as a feature, but the full path of a trip. Therefore all the feature evaluation from the earlier approach could be applied here as well. The weird thing was, that including the other features didn't give us better results.

\subsection{Single k-Nearest neighbour, complex features}

The next idea was to return to single k-nearest neighbor just with starting point, get 5000 results of it and then use our own similarity function and average first 50 most similar results. In this function we tried two things, feature \texttt{polyline\_shape} and \texttt{polyline\_time\_cut}. Difference (which was recalculated to similarity) was then defined as sum of distances between points in test and train trip after each $X$ meters ($X = 500$) for feature \texttt{polyline\_shape} and after each $Y$ seconds with $Y = 120$.

\begin{table}[h]
\centering
\caption{Results of k-NN with complex features}
\label{multikd_result}
\begin{tabular}{llll}
Feature   & Kaggle evaluation \\ \hline
\texttt{polyline\_time\_cut}  & 0.73421              \\
\texttt{polyline\_shape}  &  0.75860        \\
\end{tabular}
\end{table}

Because this approach went to significantly worse results than using multi kd-tree model, we did not continue in improving it, even though our visualization looked very good. Then we focused on getting the best possible result with previous approach.

\subsection{Multiple k-nearest neighbour and decision tree}

Our last try to get our regressor better was to combine multiple k-nearest neighbor regressor with decision tree algorithm. The idea was to get our regressor better for the shortest test trips. For decision trees we used these features with the same weight (there was no point to add any other features, because in some cases is polyline empty):

\begin{itemize}
    \item Call type
    \item Traffic map
\end{itemize}

The minimum leaf size was set to 10.000 items and we tried to find a threshold, which will tell how short the test trip have to be to use decision tree.

We tried all thresholds from 0 to longest test trip and the best threshold was 2. However, only small amount of the test trips have this small cut, so the final result would be nearly the same (but a littlebit worse by approximately 0.02) as our best submission.

\section{Conclusion}
In this challenge we tried many different approaches to get the right features, most of them using k-nearest neighbor speeded-up with kd-tree algorithm. 

One of main things which affected the results was the data analysis at the start. A possible improvement could be to find and drop very atypical trips from our train data, from visualization we know that some trips looks like an ellipse (maybe the taxi driver forgot to restart the GPS), some trips have surprisingly long distance between two points. We also could have dropped the most similar trips which did not give us any added value and then use smaller number of neighbours to average. By this we could have avoided picking random 400.000 train trips and pick only the "interesting-ones". This could have affected the speed as well as precision. 

As the best approach we picked our "multiple kd-tree regressor".

Breaking "the rule" of machine learning could have been also the key. After the contest was finished we read in the kaggle forum, that somebody analyzed the test set and found out that there are only trips at a certain time. He used this to pick a right training set. 

All in all this was a really cool challenge. Somehow disturbing, because nothing really showed an effect, but fun. 
%----------------------------------------------------------------------------
% BEGIN Literaturverzeichnis
\bibliographystyle{plain}
%\nocite{*}
\bibliography{Template}
% END Literaturverzeichnis
%----------------------------------------------------------------------------

\end{document}