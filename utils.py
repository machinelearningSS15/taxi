#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
from datetime import datetime

def calculate_initial_compass_bearing(pointA, pointB):
    """
    Calculates the bearing between two points.

    :Parameters:
      - `pointA: The tuple representing the latitude/longitude for the
        first point. Latitude and longitude must be in decimal degrees
      - `pointB: The tuple representing the latitude/longitude for the
        second point. Latitude and longitude must be in decimal degrees

    :Returns:
      The bearing in degrees

    :Returns Type:
      float
    """
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")
 
    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])
 
    diffLong = math.radians(pointB[1] - pointA[1])
 
    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
            * math.cos(lat2) * math.cos(diffLong))
 
    initial_bearing = math.atan2(x, y)
 
    # Now we have the initial bearing but math.atan2 return values
    # from -180 to + 180 which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360
 
    return compass_bearing
    
def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    Returns distance in meters
    http://stackoverflow.com/questions/15736995/how-can-i-quickly-estimate-the-distance-between-two-latitude-longitude-points
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
    c = 2 * math.asin(math.sqrt(a)) 
    km = 6367 * c
    return km * 1000

def get_labels_by_call_type(features):
    A = []
    B = []
    C = []
    
    for feature in features:
        label = (len(feature["polyline"]) - 1) * 15
        if feature["call_type"] == "A":
            A.append(label)
        elif feature["call_type"] == "B":
            B.append(label)
        elif feature["call_type"] == "C":
            C.append(label)
        
    return [A, B, C]

def get_labels_by_day_type(features):
    A = []
    B = []
    C = []
    
    for feature in features:
        label = (len(feature["polyline"]) - 1) * 15
        if feature["day_type"] == "A":
            A.append(label)
        elif feature["day_type"] == "B":
            B.append(label)
        elif feature["day_type"] == "C":
            C.append(label)
        
    return [A, B, C]

def generateTrafficMap(features, parts):    
    trafficMap = {}

    if parts > 1:
        trafficMap = {}
        speeds, distances, times, counts_trips = get_average_speeds_during_day(features)
        
        max_times = max(times)
        min_times = min(times)
        step = (max_times - min_times) / (parts - 1)
        
        for index, time in enumerate(times):
            trafficMap[index] = round((time - min_times) / step)
            
        #print trafficMap
    else:
        for index in range(23):
            trafficMap[index] = 0.0 
    return trafficMap

def get_average_speeds_during_day(features):
    """
        returns array of average speeds (24 hours), array of average distances and array of count of trips during each hour
    """
    
    speeds = [0.0] * 24
    distances = [0.0] * 24
    times = [0.0] * 24
    counts_trips = [0] * 24
    
    for feature in features:
        hour = datetime.fromtimestamp(feature["timestamp"]).hour
        
        distance = 0.0
        for index, current_point in enumerate(feature["polyline"]):
            # skip first point
            if index == 0:
                continue
            
            previous_point = feature["polyline"][index - 1]
            
            distance += haversine(previous_point[0], previous_point[1], current_point[0], current_point[1])
        
        distances[hour] += distance    
        speeds[hour] += distance * 1.0 / ((len(feature["polyline"]) - 1) * 15)
        counts_trips[hour] += 1
        times[hour] += (len(feature["polyline"]) - 1) * 15
        
    # calculate the average
    for index, speed in enumerate(speeds):
        speeds[index] = speed / counts_trips[index]
        distances[index] = distances[index] / counts_trips[index]
        times[index] = times[index] / counts_trips[index]
    return speeds, distances, times, counts_trips

def duration_from_polyline(polyline):
    """
        returns the duration of a polyline, the way kaggle does
    """
    
    return (len(polyline)-1)*15

def format_duration(seconds):
    """
    Formats duration into human readble format
    @type seconds: int
    @param seconds: number of seconds

    Return type: string
    Returns: HH:mm:SS, or mm:SS
    """

    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    if h != 0:
        return "%dh%02d'%02d''" % (h, m, s)
    else:
        return "%02d'%02d''" % (m, s)
