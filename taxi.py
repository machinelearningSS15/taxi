#!/usr/bin/env python
# -*- coding: utf-8 -*-

from regressor_io import read_taxi_data, read_split_taxi_data
from nearestneighborregressor import NearestNeighborRegressor
from decisiontreeregressor import DecisionTreeRegressor
from multiplekdtreeregressor import MultipleKDTreeRegressor
from regressortester import RegressorTester
from visualizer import *
import matplotlib.pyplot as plt
from utils import get_labels_by_call_type, get_labels_by_day_type,\
    get_average_speeds_during_day

TRAIN_DATA_SIZE = 400000
TEST_DATA_SIZE = 2000 
AMOUNT_OF_CROSS_VALIDATION_SPLITS = 3


if __name__ == "__main__":
    
    # read input data
    (trainfeatures, trainlabels,
     testfeatures, testlabels) = read_split_taxi_data("data/train.csv", TRAIN_DATA_SIZE, TEST_DATA_SIZE, AMOUNT_OF_CROSS_VALIDATION_SPLITS, 0)
    
    #Only uncomment if you want a testscript for kaggle
    # (testfeatures, testlabels) = read_taxi_data("data/test.csv")
    
    ##########################
    ## Different algorithms ##
    ##########################

    #############################
    ## SINGLE NEAREST NEIGHBOR ##
    #############################
    # m = NearestNeighborRegressor(5000, 50)
    # m.train(trainfeatures, trainlabels)
    # results = m.apply(testfeatures)

    # tester = RegressorTester(testfeatures, testlabels, results)
    # visualize(m.neighbourtrips, testfeatures, testlabels, results,  tester.getQuality(), openBrowser=True)



    ###################
    ## Multi KD Tree ##
    ###################
    m = MultipleKDTreeRegressor(50,5000)
    m.train(trainfeatures, trainlabels)
    results = m.apply(testfeatures)
    
    tester = RegressorTester(testfeatures, testlabels, results)
    visualize(m.neighbourtrips, testfeatures, testlabels, results,  tester.getQuality(), openBrowser=True)



    ######################################
    ## MUTLIPLE KD TREE + DECISION TREE ##
    # ######################################
    # m = MultipleKDTreeRegressor()
    # m.train(trainfeatures, trainlabels)
    # multipleKD_results = m.apply(testfeatures)
    
    # d = DecisionTreeRegressor(10000)
    # d.train(trainfeatures, trainlabels)
    # decision_results = d.apply(testfeatures)


    # # generate threshold range
    # max_polyline = 0
    # #for testfeature in testfeatures:
    # #    if testfeature["polyline"] != None and len(testfeature["polyline"]) > max_polyline:
    # #        max_polyline = len(testfeature["polyline"])

    # #for threshold in range(max_polyline):
    # threshold = 2 # we know that best threshold is 2
    # results = []
    # for index, multipleKD_result in enumerate(multipleKD_results):
    #     if testfeatures[index]["polyline"] == None or len(testfeatures[index]["polyline"]) <= threshold:
    #         results.append(decision_results[index])
    #     else:
    #         results.append(multipleKD_result)

    # tester = RegressorTester(testfeatures, testlabels, results)

    # print threshold, tester.getQuality()
    

    ##########################################
    ## If you want a kaggle submission file ##
    ##########################################
    #tester.generateSubmissionFile("data/submission.csv")
    #tester.generateSubmissionFile("data/decisiontreesubmission.csv")


    ##########################################
    ## If you want a multiple train sets    ##
    ##########################################
    """
    for i in range(0,AMOUNT_OF_CROSS_VALIDATION_SPLITS):
        # read input data
        (trainfeatures, trainlabels,
         testfeatures, testlabels) = read_split_taxi_data("data/train.csv", TRAIN_DATA_SIZE, TEST_DATA_SIZE, AMOUNT_OF_CROSS_VALIDATION_SPLITS, i)
        
        # (testfeatures, testlabels) = read_taxi_data("data/test.csv")
         
        #for i in range(20):  
        #print "neighbours:" + str((i + 4) * 5)
        
        
        #m = NearestNeighborRegressor(2000)
        #m.train(trainfeatures, trainlabels)
        #results = m.apply(testfeatures)

        m = MultipleKDTreeRegressor(50,5000)
        m.train(trainfeatures, trainlabels)
        results = m.apply(testfeatures)
        
        tester = RegressorTester(testfeatures, testlabels, results)
        
        print tester.getQuality()
        visualize(m.neighbourtrips, testfeatures, testlabels, results,  tester.getQuality(), openBrowser=True)
        break
        #tester.generateSubmissionFile("data/submission.csv")
    """

