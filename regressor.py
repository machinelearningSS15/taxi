class Regressor:
    def __init__(self):
        self.model = None

    def train(self, features, labels):
        """
            trains Regressor
            @type features: list
            @param features: 'features' contains array in following form:
                            [{'taxi_id': '20000589', 
                            'polyline': [[-8.618643, 41.141412], [-8.618499, 41.141376], ...], 
                            'day_type': 'A', 
                            'origin_call': '', 
                            'origin_stand': '', 
                            'missing_data': False, 
                            'timestamp': 1372636858, 
                            'trip_id': '1372636858620000589', 
                            'call_type': 'C'}, ...]
            @type labels: list
            @param labels: list of labels ( = computed duration in seconds), e.c.:
                            [5613, 3218, ...]
        """
        raise NotImplementedError()
    
    def apply(self, features):
        """
            runs Regressor on testing data
            @type features: list
            @param features: 'features' contains array in following form:
                            [{'taxi_id': '20000589', 
                            'polyline': [[-8.618643, 41.141412], [-8.618499, 41.141376], ...], 
                            'day_type': 'A', 
                            'origin_call': '', 
                            'origin_stand': '', 
                            'missing_data': False, 
                            'timestamp': 1372636858, 
                            'trip_id': '1372636858620000589', 
                            'call_type': 'C'}, ...]
            @rtype: list
            @return: array of tuples containing id and computed duration in seconds:
                    [('1372636858620000589', 13215), (..., ...), ...]
        """
        raise NotImplementedError()
    
