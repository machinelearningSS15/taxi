import folium
import json 
import math
import csv
import colorsys
import pandas as pd
from pylab import *
from utils import *
from optparse import OptionParser
import webbrowser
import os
from urllib import pathname2url


def print_polyline(map_osm, tripData, tripDataTime = None, point_color = "#ECE504", path_color='red', start_point_radius = 200, start_point_color='lightblue', end_point_color='green', polygon_marker=False):
    """
    Prints one trip to a given folium map.
    @type map_osm: object
    @param map_osm: a folium map object
    @type tripData: dict
    @param tripData: data for one trip

    @type tripDataTime: int
    @param tripDataTime: time for trip in seconds. if None then its assumed that the time == len(polyline-1)

    Rest: Some formating options with presets.

    Return type: object
    Returns modified map object
    """

    coordinates = [[point[1], point[0]] for point in tripData["polyline"]]

    #print the points
    tmp = tripData.pop("polyline")
    
    
    duration = 0
    for point in coordinates:
        if polygon_marker or (duration/60) in [5,10,15]:
            time = format_duration(duration)
            map_osm.polygon_marker(location=point, popup=time, fill_color=point_color, num_sides=30, fill_opacity=0.2, radius=7)
        duration+=15
    duration -= 15

    if tripDataTime:
        duration = tripDataTime

    #print line                    
    map_osm.line(coordinates, line_color=path_color, line_weight=8)
    
    #draw startpoint
    map_osm.circle_marker(location=coordinates[0], radius=start_point_radius,
                    popup=str(tripData), line_color=start_point_color,
                    fill_color=start_point_color, fill_opacity=0.6)
    
    #mark endpoint    
    tid = "Unknown"
    if "trip_id" in tripData:
        tid = tripData["trip_id"]
    if "difference" not in tripData:
        tripData["difference"] = "Unknown"

    map_osm.circle_marker(location=coordinates[-1], radius=50,
                    popup='End Point! \nTrip ID = '+ tid + ' \n Duration = ' + format_duration(duration)+
                     ' \n Difference = ' + tripData["difference"], line_color=end_point_color,
                    fill_color=end_point_color)
    
    tripData["polyline"] = tmp

    return map_osm
    

def clamp(x): 
    return max(0, min(x, 255))

def get_color_for_height(height, row, start_color = [0,0,0], end_color= [255,100,255]):
    """
    Makes some kind of color gradient for height items 
    @type height: int
    @param height: number of items for the gradient (eg: 25)
    @type row: int
    @param row: item number for the desired element.  
    @type start_color: list
    @param start_color: List with rgb values for the beginning of the gradient
    @type end_color: list
    @param end_color: List with rgb values for the end of the gradient

    Return type: string
    Returns corresponding hexadecimal color 
    """

    scale = float(row) / height

    r = start_color[0] + scale * (end_color[0] - start_color[0])
    g = start_color[1] + scale * (end_color[1] - start_color[1])
    b = start_color[2] + scale * (end_color[2] - start_color[2])
    #print [r,g,b]
    return "#{0:02x}{1:02x}{2:02x}".format(clamp(int(r)), clamp(int(g)), clamp(int(b)))



def print_multiple_trips(listOfTrips, compareTrip = None, compareTripTime = None, filename='osm'):
    """
    Creates filename + .html with nearest neighbor directions.
    @type list_of_trips: list
    @param list_of_trips: list of nearest neighbors with full features.
    @type compare_trip: dict
    @param compare_trip: the compared trip for which the nearest neighbors were found.
    @type filename: string
    @param filename: output filename without .html. preset creates osm.html 
    """
   #print "REAL trip time:", compareTripTime

    #find out how diffrent the neighbors are
   
    listOfTrips.sort(key = lambda a: abs(duration_from_polyline(a["polyline"])-compareTripTime))

    #calculate color, depending on distance of duration


    #print min(enumerate(x), key=lambda a: abs(a[1]-compareTripTime))     

    loc=[listOfTrips[0]["polyline"][0][1], listOfTrips[0]["polyline"][0][0]]
    map_osm = folium.Map(location=loc, tiles='Stamen Toner', zoom_start=14)



    #visualize the nearest neighbors
    for i, trip in enumerate(listOfTrips):
        trip["difference"] = str(abs(duration_from_polyline(trip["polyline"])-compareTripTime))
        #print "Neighbor is", trip["difference"], "seconds different"
        map_osm = print_polyline(map_osm, trip, path_color=get_color_for_height(len(listOfTrips), i, start_color=[0,255,0], end_color=[255,0,0]))
        #break
    #if you passed a test trip 
    if compareTrip:
        #print CROPPED and UNCROPPED POLYLINE of test trip
        map_osm = print_polyline(map_osm, compareTrip, tripDataTime = compareTripTime, path_color = "yellow", start_point_color = "orange", start_point_radius=300, end_point_color="orange")
        compareTrip["polyline"] = compareTrip["polyline_uncropped"]
        map_osm = print_polyline(map_osm, compareTrip, tripDataTime = compareTripTime, path_color = "blue", start_point_color = "orange", start_point_radius=0, end_point_color="blue")

    #create the output file
    map_osm.create_map(path=filename+'.html')
    fix_folium_output_file(filename+'.html')
    #print "Printed " + str(len(listOfTrips)) + " nearest trips in " + filename +'.html'

#in the folium html file are some errors. we need to fix them
def fix_folium_output_file(filename):
    """
    Takes folium output file and replaces all wrong links
    @type filename: string
    @param filename: folium map filename with .html. 
    """

    with open(filename, 'r') as in_file:
        text = in_file.read()

    with open(filename, 'w') as out_file:
        "width: 960px; height: 500px"
        text = text.replace('src="//', 'src="http://')
        text = text.replace("width: 960px; height: 500px", "width: 1280px; height: 720px")
        out_file.write(text.replace('href="//', 'href="http://'))


def print_direction_of_trips(list_of_trips, compare_trip, compareTripTime = None, filename='osm', show_taxi_stands=True):
    """
    Creates filename + directions.html with nearest neighbor directions.
    @type list_of_trips: list
    @param list_of_trips: list of nearest neighbors with full features.
    @type compare_trip: dict
    @param compare_trip: the compared trip for which the nearest neighbors were found.
    @type filename: string
    @param filename: output filename without .html. preset creates osmdirections.html 
    """

    loc=[list_of_trips[0]["polyline"][0][1], list_of_trips[0]["polyline"][0][0]]
    map_osm = folium.Map(location=loc, tiles='Stamen Toner', zoom_start=14)


    if show_taxi_stands:
        map_osm = print_taxi_stands(map_osm)


    for i, trip in enumerate(list_of_trips):
        map_osm = print_polyline(map_osm, {"polyline": [[trip["polyline"][0][0],trip["polyline"][0][1]], [trip["polyline"][-1][0],trip["polyline"][-1][1]]], "trip_id": trip["trip_id"]}, tripDataTime = (len(compare_trip["polyline"]) - 1)*15,  path_color=get_color_for_height(len(list_of_trips), i))
 
    if compare_trip:
        map_osm = print_polyline(map_osm, {"polyline": [[compare_trip["polyline"][0][0],compare_trip["polyline"][0][1]], [compare_trip["polyline"][-1][0],compare_trip["polyline"][-1][1]]]}, tripDataTime = compareTripTime,path_color = "green", start_point_color = "orange", start_point_radius=300, end_point_color="orange")
 
    map_osm.create_map(path=filename+'directions.html')
    fix_folium_output_file(filename+'directions.html')
    print "Printed " + str(len(list_of_trips)) + " nearest trips in " + filename +'directions.html'
    #combine_files(filename +'.html',filename +'directions.html', "out.html")


def visualize(neighbourtrips, testfeatures, testlabels, results, last_quality, openBrowser=False):
    



    f = open('out/index.html','w')

    message = """   <!DOCTYPE html>
                    <html>
                    <head>
                    <script>
                        var selectedrow = 0;
                        function setURL(url, i){
                            document.getElementById('iframe').src = url;
                            document.getElementById(selectedrow).style.backgroundColor = "white";
                            document.getElementById(i).style.backgroundColor = "grey";
                            selectedrow=i;
                        }
                    </script>
                    </head>
                    <body>
                    <h1>"""
    message += str(last_quality)
    message += """</h1>
                    <table>
                      <tr>
                        <th>Test Trips</th>
                        <th id="maptitle">Map</th>
                      </tr>
                      <tr>
                        <td width="15%" ><div style="height:720px; overflow: auto;"><table>"""

    for testNumber, realduration in enumerate(testlabels):
        #print "#### TEST", testNumber, "####"
        print_multiple_trips(neighbourtrips[testNumber], testfeatures[testNumber], compareTripTime = testlabels[testNumber], filename="out/"+str(testNumber)+"map")
        #print_direction_of_trips(neighbourtrips[testNumber], testfeatures[testNumber], compareTripTime = testlabels[testNumber], filename="out/"+str(testNumber))
        
        message += """  <tr id={0}>
                            <td>{0} </td>
                            <td><button type="button" onclick="setURL('{0}map.html', {0})"> Map </button></td>
                            <td><b>{1}</b></td> 
                            <td><i>{2}</i></td>
                        </tr> """.format(str(testNumber), format_duration(realduration), format_duration(results[testNumber][1]))


        #message += """  <tr id={0}>
        #            <td>{0} </td>
        #            <td><button type="button" onclick="setURL('{0}map.html', {0})"> Map </button></td>
        #            <td><b>{1}</b></td> 
        #            <td><i>{2}</i></td>
        #            </tr> """.format(str(testNumber), format_duration(realduration), format_duration(results[testNumber][1]))


    message += """</table></div></td>
                        <td width="70% align:"center" style="vertical-align:top"">  <iframe id="iframe" src="0map.html" width="1280" height="720" />
                    </td>
                  </tr>
                </table>
                <input type="hidden" name="Language" value="-1map.html">
                </body>
                </html>"""

    f.write(message)
    f.close()

    # open generated visualization
    if openBrowser:
        url = 'file:{}'.format(pathname2url(os.path.abspath('out/index.html')))
        webbrowser.open(url)


def print_taxi_stands(map_osm, filename="data/metaData_taxistandsID_name_GPSlocation.csv"):
    data = pd.read_csv(filename)
    #print data 


    return map_osm

def plot_histogram(title, xlabel, values, bins = 10):    
    # the histogram of the data
    n, bins, patches = plt.hist(values, bins, normed=True, facecolor='green', alpha=0.75)
    
    plt.xlabel(xlabel)
    plt.ylabel('Probability')
    plt.title(r'$\mathrm{' + title + '}$')
    plt.grid(True)
    
    plt.savefig("out/" + title + ".jpg")
    plt.close()
    #plt.show()
    
def plot_averages(features):
    speeds, distances, times, trip_counts = get_average_speeds_during_day(features)
    
    plt.plot(distances)
    plt.ylabel('average distance')
    plt.xlabel('hour of a day')
    plt.savefig("out/average_distance.jpg")
    plt.close()
    
    plt.plot(times)
    plt.ylabel('average time')
    plt.xlabel('hour of a day')
    plt.savefig("out/average_time.jpg")
    plt.close()
    
    plt.plot(speeds)
    plt.ylabel('average speed')
    plt.xlabel('hour of a day')
    plt.savefig("out/average_speed.jpg")
